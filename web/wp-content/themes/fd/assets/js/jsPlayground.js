
var BaseConverter = {
    outputChars: [
      "0",
      "1",
      "2",
      "3",
      "4",
      "5",
      "6",
      "7",
      "8",
      "9",
      "A",
      "B",
      "C",
      "D",
      "E",
      "F"
    ],
    convert: function(number, base) {
      var digits =
        number < 2
          ? 1
          : Math.ceil(Math.log(parseInt(number) + 1) / Math.log(parseInt(base)));
      var result = [];
      for (var i = digits - 1; i >= 0; i--) {
        result[i] = BaseConverter.outputChars[number % base];
        number = parseInt(number / base);
      }
      return result.join("");
    }
  };
  

$(document).ready(function () {
    $("#baseCalcForm").on("submit", function () {
        var number = $("#number").val();
        var base = $("#base").val();

        $("#result").html(BaseConverter.convert(number, base));
        return false;
    }).trigger("submit");
});