# WordPress Development Environment for Docker
This directory contains a Docker Compose configuration for a special LAMP-Stack intended for WordPress development. For easier database management, it also provides an instance of [phpMyAdmin](https://www.phpmyadmin.net/).

**Do not use this configuration for a production environment!**

# Usage
- Place your WordPress files inside the `web` directory. If starting from scratch, have a look [here](https://wordpress.org/download/).
- Starting your development environment is as simple as running `docker-compose up -d` from your terminal, while having your current working directory point to this directory.
- To stop the environment execute `docker-compose stop`.
- Open [http://localhost:8080](http://localhost:8080) to visit your site.
- In order to access phpMyAdmin, visit [http://localhost:8085](http://localhost:8085).
- The default password for the MariaDB root user is `docker`.
- See [wordpress.dockerfile](wordpress.dockerfile) for examples on how to add PHP extensions. **Only install packages and extensions that are really needed, otherwise they will bloat the container and slowdown the build process!**
- **Note:** Modifying the Dockerfile requires a rebuild of the web container. This can be accomplished by issuing `docker-compose build`.

## Database import / export
- For quickly **exporting** the current state of the database (without using phpMyAdmin) run `docker-compose exec db mysqldump --add-drop-table -u root -pdocker wordpress > wordpress.sql`.
- An **import** can be accomplished by running `docker-compose exec -T db mysql -u root -pdocker wordpress < wordpress.sql`.  
**Warning: this will overwrite your existing database!**