FROM wordpress:latest

# Below you find examples on how to install additional system packages and php extensions.
# The wordpress image already installs the core dependencies

# # install additional system packages
# RUN apt update && apt install -y --no-install-recommends \
# # library required by imagick
# libmagickwand-dev \
# # library required by intl
# libicu-dev && \
# # Install and enable php packages
# pecl install imagick && \
# docker-php-ext-install intl gettext mysqli && \
# docker-php-ext-enable imagick
RUN chown -R www-data:www-data /var/www/html/